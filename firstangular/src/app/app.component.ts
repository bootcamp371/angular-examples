import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Mark\'s first Angular project';
  h3 = "This is my h3 header";
  myNumber: number = 2.3456;
  sayHello(name: string): string {
    console.log(`Hello, ${name}!`);
    return name;
  }
}
