import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'counter-app';
  today: string = "";
  greeting: string = "";
  ngOnInit() {
    let rightNow = new Date();
    let theHour: number = 0;
    this.today = rightNow.toLocaleDateString();
    theHour = rightNow.getHours();
    if (theHour >= 0 && theHour <= 4){
      this.greeting = "It's deep night - why are you awake?";
    } 
    else if (theHour >= 5 && theHour <= 11){
      this.greeting = "It's a wonderful morning";
    }
    else if (theHour >= 12 && theHour <= 17){
      this.greeting = "It's a great afternoon";
    }
    else if (theHour >= 18 && theHour <= 23){
      this.greeting = "It's a nice evening";
    }
  }
}
